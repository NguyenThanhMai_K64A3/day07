<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  
  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="./style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
</head>
<body>
    <form method="post" enctype="multipart/form-data" action=" <?php 
      echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
      <!-- khoa -->
      <div class="title">
        <label class="label-list khoa" for="khoa">Khoa</label><br>
        <select class="khoa" name="khoa" id="khoa">
          <option value=""></option>
          <?php
            $khoa = array('MAT'=>'Khoa học máy tính', 'KDL'=>'Khoa học vật liệu');
            foreach($khoa as $key => $value):
              echo '<option value="'.$value.'">'.$value.'</option>';
            endforeach;
          ?>
        </select>
      </div>

      <!-- keyword -->
      <div class="title">
        <label class="label-list text" for="keyword">Từ khóa</label><br>
        <div><input class="username" type="text" id="keyword" name="keyword"><br></div>
      </div>

      <!-- button submit -->
      <button type="submit" name="submit" class="search">Tìm kiếm</button>

  <p class="count_student" style="text-align: left;">Số sinh viên tìm được: XXX</p>

  <!-- button add -->
  <a href="./register.php">
    <input type="button" value="Thêm" class="add">
  </a>

  <!-- List students -->
  <table>
    <tr>
      <th>No</th>
      <th>Tên sinh viên</th>
      <th>Khoa</th>
      <th>Action</th>
    </tr>
    <tr>
      <td>1</td>
      <td>Nguyễn Văn A</td>
      <td>Khoa học máy tính</td>
      <td>
        <button class="action edit">Sửa</button>
        <button class="action delete">Xóa</button>
      </td>
    </tr>
    <tr>
    <td>2</td>
      <td>Trần Thị B</td>
      <td>Khoa học máy tính</td>
      <td>
        <button class="action edit">Sửa</button>
        <button class="action delete">Xóa</button>
      </td>
    </tr>
    <tr>
    <td>3</td>
      <td>Hoàng Văn C</td>
      <td>Khoa học dữ liệu</td>
      <td>
        <button class="action edit">Sửa</button>
        <button class="action delete">Xóa</button>
      </td>
    </tr>
    <tr>
    <td>4</td>
      <td>Đinh Quang D</td>
      <td>Khoa học dữ liệu</td>
      <td>
        <button class="action edit">Sửa</button>
        <button class="action delete">Xóa</button>
      </td>
    </tr>
  </table>
    </form>
</body>

</html>
